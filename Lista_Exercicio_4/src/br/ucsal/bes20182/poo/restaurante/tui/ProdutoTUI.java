package br.ucsal.bes20182.poo.restaurante.tui;

import java.util.Scanner;

import br.ucsal.bes20182.poo.restaurante.business.ProdutoBO;
import br.ucsal.bes20182.poo.restaurante.domain.Produto;

public class ProdutoTUI {
	public Scanner sc = new Scanner(System.in);
	public ProdutoBO produtoBO = new ProdutoBO();
	
	public void adicionar() {
		String nome = obterString("Nome do item: ");
		Integer codigo = obterInt("Codigo do item: ");
		String descricao = obterString("Informe a descricao do item: ");
		Double valor = obterDouble("Valor do item: ");
		Produto produto = new Produto(nome, descricao, valor, codigo);
		String erro = produtoBO.adicionar(produto);
		if(erro !=null) {
			System.out.println(erro);
		}
		
	}

	private Double obterDouble(String mensagem) {
		System.out.println(mensagem);
		return sc.nextDouble();
	}

	private Integer obterInt(String mensagem) {
		System.out.println(mensagem);
		return sc.nextInt();
	}

	private String obterString(String mensagem) {
		System.out.println(mensagem);
		return sc.nextLine();
	}
}
