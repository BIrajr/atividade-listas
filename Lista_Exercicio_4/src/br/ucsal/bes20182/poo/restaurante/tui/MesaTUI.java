package br.ucsal.bes20182.poo.restaurante.tui;

import java.util.Scanner;

import br.ucsal.bes20182.poo.restaurante.business.MesaBO;
import br.ucsal.bes20182.poo.restaurante.domain.Mesa;



public class MesaTUI {
	public Scanner sc = new Scanner(System.in);
	public MesaBO mesaBO= new MesaBO();
	
	public void cadastrar() {
		Integer numero = obterInt("Informe o numero da mesa: ");
		sc.nextLine();
		Integer capacidade = obterInt("Informe a capacidade da mesa: ");
		sc.nextLine();
		Mesa mesa = new Mesa(numero, capacidade);
		mesaBO.cadastrar(mesa);
	}
	
	private Integer obterInt(String mensagem) {
		System.out.println(mensagem);
		return sc.nextInt();
	}

	public void selecionar() {
		System.out.println("Mesa para quantas pessoas ?");
		Integer quant  = sc.nextInt();
		String resp = "";
		Integer numMesa = mesaBO.selecionar(quant);
		if(numMesa != null) {
		System.out.println("Mesa " + numMesa + "esta liberada: ");
		mesaBO.ocupar(numMesa, quant);
			while(resp!= null) {
				System.out.println("Deseja anotar um pedido? ");
				resp = sc.nextLine();
				if(resp.equals("S") || resp.equals("Sim") || resp.equals("sim") || resp.equals("SIM") ) {
					System.out.println("Qual o pedido do cliente? ");
					String pedido = sc.nextLine();
					mesaBO.anotar(numMesa, pedido);
				}
				else {
					resp = null;
				}
			}
		}
		else {
			System.out.println("Desculpe! Nenhuma mesa disponivel no momento");
		}
	}
	
	public void desocupar(Integer numMesa) {
		mesaBO.desocupar(numMesa);
	}
	
}
