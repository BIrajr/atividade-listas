package br.ucsal.bes20182.poo.restaurante.business;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20182.poo.restaurante.domain.Produto;
import br.ucsal.bes20182.poo.restaurante.persistence.ProdutoDAO;

public class ProdutoBO {
	ProdutoDAO produtoDAO = new ProdutoDAO();
	
	public String adicionar(Produto produto) {
		List <Produto> produtos = new ArrayList<>();
		produtos = produtoDAO.retornaList();
		String erro = verificar(produtos, produto);
		if(erro == null) {
			produtoDAO.adicionar(produto);
			return erro;
		}
		else {
			return erro;
		}
	}

	private String verificar(List<Produto> produtos, Produto produto) {
		for(int i = 0;i<produtos.size();i++) {
			if(produto.getCodigo() == produtos.get(i).getCodigo()) {
				return "Itens com codigo iguais! Favor recadastrar Item";
			}
		}
		return null;
	}
}
