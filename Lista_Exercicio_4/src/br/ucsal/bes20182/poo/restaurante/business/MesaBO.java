package br.ucsal.bes20182.poo.restaurante.business;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20182.poo.restaurante.domain.Mesa;
import br.ucsal.bes20182.poo.restaurante.persistence.MesaDAO;

public class MesaBO {
	MesaDAO mesaDAO = new MesaDAO();
	
	public String cadastrar(Mesa mesa) {
		
		List <Mesa> mesas = new ArrayList<>();
		mesas = mesaDAO.retornaList();
		String erro = verificar(mesas, mesa);
		if(erro == null) {
			mesaDAO.cadastrar(mesa);
			return erro;
		}
		else {
			return erro;
		}
	}

	private String verificar(List<Mesa> mesas, Mesa mesa) {
		for(int i = 0;i<mesas.size();i++) {
			if(mesa.getNumero() == mesas.get(i).getNumero()){
				return "Mesa com numeros iguais! Favor recadastrar mesa";
			}
		}
		return null;
	}
	
	public Integer selecionar(Integer quant) {
		Mesa mesa = mesaDAO.retornaMesaVazia(quant);
		return mesa.getNumero();
		
	}
	public void ocupar(Integer numMesa, Integer quant) {
		mesaDAO.ocupar(numMesa, quant);
		}
	public void desocupar(Integer numMesa) {
		mesaDAO.desocupar(numMesa);
	}
	public void anotar(Integer numMesa, String pedido) {
		mesaDAO.anotar(numMesa, pedido);
	}
}

