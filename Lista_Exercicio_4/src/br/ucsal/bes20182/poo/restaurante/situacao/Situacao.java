package br.ucsal.bes20182.poo.restaurante.situacao;

public enum Situacao {
	LIBERADA, OCUPADA, MANUTENCAO;
}
