package br.ucsal.bes20182.poo.restaurante.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20182.poo.restaurante.domain.Produto;

public class ProdutoDAO {
	public List<Produto> produtos = new ArrayList<>();
	public void adicionar(Produto produto) {
		produtos.add(produto);
	}
	public List<Produto> retornaList(){
		return produtos;
	}
}
