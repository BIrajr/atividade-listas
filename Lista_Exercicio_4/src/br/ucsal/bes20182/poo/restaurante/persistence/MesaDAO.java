package br.ucsal.bes20182.poo.restaurante.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20182.poo.restaurante.domain.Mesa;
import br.ucsal.bes20182.poo.restaurante.domain.Produto;
import br.ucsal.bes20182.poo.restaurante.situacao.Situacao;



public class MesaDAO {
	public List<Mesa> mesas = new ArrayList<>();
	
	public void cadastrar(Mesa mesa) {
		mesas.add(mesa);
	}
	public List<Mesa> retornaList(){
		return mesas;
	}
	public Mesa retornaMesaVazia(Integer quant){
		for(int i = 0;i < mesas.size();i++) {
		if(mesas.get(i).getSituacao() == Situacao.LIBERADA && mesas.get(i).getCapacidade() >= quant) {
			return mesas.get(i);
		}
		}
		return null;
	}
	public void ocupar(Integer numMesa, Integer quant) {
		mesas.get(numMesa-1).setSituacao(Situacao.OCUPADA);
		mesas.get(numMesa- 1).setCapacidade(quant);
	}
	
	public void desocupar(Integer numMesa) {
		mesas.get(numMesa-1).setSituacao(Situacao.MANUTENCAO);
		mesas.get(numMesa-1).setPedidos(null);
		mesas.get(numMesa-1).setSituacao(Situacao.LIBERADA);
	}
	
	public void anotar(Integer numMesa, String pedido) {
		ProdutoDAO produtoDAO =new ProdutoDAO();
		List<Produto> produtos = produtoDAO.retornaList();
		for(int i = 0;i<produtos.size();i++) {
			if(produtos.get(i).getNome().equals(pedido)) {
				mesas.get(numMesa-1).getPedidos().add(produtos.get(i));
			}
		}
	}
}
