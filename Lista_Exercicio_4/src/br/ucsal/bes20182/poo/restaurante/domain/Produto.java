package br.ucsal.bes20182.poo.restaurante.domain;

public class Produto {
	private String nome;
	private String descricao;
	private Double valor;

	public Produto(String nome, String descricao, Double valor, Integer codigo) {
		super();
		this.nome = nome;
		this.descricao = descricao;
		this.valor = valor;
		this.codigo = codigo;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	private Integer codigo;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
}
