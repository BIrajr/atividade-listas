package br.ucsal.bes20182.poo.restaurante.domain;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20182.poo.restaurante.situacao.Situacao;

public class Mesa {
	private Integer numero;
	private Integer capacidade;
	private Situacao situacao = Situacao.LIBERADA;
	private List<Produto> pedidos = new ArrayList<>();

	public List<Produto> getPedidos() {
		return pedidos;
	}

	public void setPedidos(Produto pedido) {
		this.pedidos.add(pedido);
	}

	public Mesa(Integer numero, Integer capacidade) {
		this.numero = numero;
		this.capacidade = capacidade;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Integer getCapacidade() {
		return capacidade;
	}

	public void setCapacidade(Integer capacidade) {
		if(capacidade >= this.capacidade ) {
			System.out.println("Mesa cheia!");
		}
		else {
		this.capacidade = capacidade;
		}
	}

	public Situacao getSituacao() {
		return situacao;
	}

	public void setSituacao(Situacao situacao) {
		this.situacao = situacao;
	}
}
