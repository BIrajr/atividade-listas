package br.ucsal.poo20182.veiculos;

public abstract class Veiculo{
	private String placa;
	private String modelo;
	private Integer anoFab;
	protected Double valor;
	
	
	public Veiculo(String placa, String modelo, Integer anoFab, Double valor) {
		this.placa = placa;
		this.modelo = modelo;
		this.anoFab = anoFab;
		this.valor = valor;
		
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public Integer getAnoFab() {
		return anoFab;
	}
	public void setAnoFab(Integer anoFab) {
		this.anoFab = anoFab;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) throws RuntimeException {
		if(valor <= 0) {
			RuntimeException ValorNaoValidoException = new RuntimeException("Valor n�o valido!");
			throw ValorNaoValidoException;
		}
		else 
		this.valor = valor;
	}
	@Override
	public String toString() {
		return "Veiculo [placa=" + placa + ", modelo=" + modelo + ", anoFab=" + anoFab + ", valor=" + valor
				+ "]";
	}
	
	
}
