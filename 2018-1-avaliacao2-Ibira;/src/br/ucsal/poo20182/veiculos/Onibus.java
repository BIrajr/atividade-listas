package br.ucsal.poo20182.veiculos;

public class Onibus extends Veiculo {
	private Integer numMax;

	public Onibus(String placa, String modelo, Integer anoFab, Double valor, TipoVeiculoEnum tipoVeiculo,Integer numMax) {
		super(placa, modelo, anoFab, valor);
		this.numMax = numMax;
	}
	public void calcularLoc() throws RuntimeException {
		valor = numMax*10D;
		if(valor <= 0) {
			RuntimeException ValorNaoValidoException = new RuntimeException("Valor n�o valido!");
			throw ValorNaoValidoException;
		} 
	}
	public Integer getNumMax() {
		return numMax;
	}

	public void setNumMax(Integer numMax) {
		this.numMax = numMax;
	}
	
	@Override
	public String toString() {
		return "Onibus [numMax=" + numMax + ", toString()=" + super.toString() + "]";
	}

}
