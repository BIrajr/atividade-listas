package br.ucsal.poo20182.veiculos;

public class Caminhao extends Veiculo {
	private Integer qtsEixos;
	private Integer capCarga;
	
	public Caminhao(String placa, String modelo, Integer anoFab, Double valor, TipoVeiculoEnum tipoVeiculo,Integer qtsEixos, Integer capCarga) {
		super(placa, modelo, anoFab, valor);
		this.qtsEixos = qtsEixos;
		this.capCarga = capCarga;
	}
	public void calcularLoc()throws RuntimeException {
		valor = capCarga*8D;
		if(valor <= 0) {
			RuntimeException ValorNaoValidoException = new RuntimeException("Valor n�o valido!");
			throw ValorNaoValidoException;
		} 
	}
	public Integer getQtsEixos() {
		return qtsEixos;
	}

	public void setQtsEixos(Integer qtsEixos) {
		this.qtsEixos = qtsEixos;
	}

	public Integer getCapCarga() {
		return capCarga;
	}

	public void setCapCarga(Integer capCarga) {
		this.capCarga = capCarga;
	}

	@Override
	public String toString() {
		return "Caminhao [qtsEixos=" + qtsEixos + ", capCarga=" + capCarga + ", toString()=" + super.toString() + "]";
	}
	
}
