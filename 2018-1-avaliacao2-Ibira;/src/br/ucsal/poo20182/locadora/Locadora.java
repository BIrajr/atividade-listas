package br.ucsal.poo20182.locadora;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.ucsal.poo20182.veiculos.Caminhao;
import br.ucsal.poo20182.veiculos.Onibus;
import br.ucsal.poo20182.veiculos.Veiculo;

public class Locadora {
	static List<Veiculo> veiculos = new ArrayList<>();
	
	public static void cadastrarOnibus(Onibus onibus) {
		onibus.calcularLoc();
		veiculos.add(onibus);
		
	}
	public static void cadastrarCaminhao(Caminhao caminhao) {
		caminhao.calcularLoc();
		veiculos.add(caminhao);
	}
	public static void listarVeiculosOrdenadoPorValor() {
		Collections.sort(veiculos, new Comparator<Veiculo>(){
			@Override
			public int compare(Veiculo veiculo1, Veiculo veiculo2) {
				 return veiculo1.getValor().compareTo(veiculo2.getValor());
				

		}
		});
		
		for (Veiculo veiculo : veiculos) {
			System.out.println(veiculo);
		}
	}
	public static void listarVeiculosOrdenadoPorPlaca() {
		Collections.sort(veiculos, new Comparator<Veiculo>(){
			@Override
			public int compare(Veiculo veiculo1, Veiculo veiculo2) {
				 return veiculo1.getPlaca().compareTo(veiculo2.getPlaca());
		}
		});
		
		for (Veiculo veiculo : veiculos) {
			System.out.println(veiculo);
		}
	}
	
}
