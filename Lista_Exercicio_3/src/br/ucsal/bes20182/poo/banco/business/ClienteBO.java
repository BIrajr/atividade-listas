package br.ucsal.bes20182.poo.banco.business;

import java.util.List;

import br.ucsal.bes20182.poo.banco.domain.Cliente;
import br.ucsal.bes20182.poo.banco.estado.EstadoConta;
import br.ucsal.bes20182.poo.banco.persistence.ClienteDAO;

public class ClienteBO {
	ClienteDAO clienteDAO = new ClienteDAO();

	public String registrar(Cliente cliente, boolean doc) {
		String valid = validar(cliente, doc);
		if (valid == null) {

			clienteDAO.registrar(cliente);
			return "Conta aberta com sucesso!";
		} else {
			return valid + " Apresente todos os documentos necessarios!";
		}
	}

	public String validar(Cliente cliente, boolean doc) {
		if (doc) {

			return null;
		} else {
			return "Conta suspensa!";
		}
	}

	public List<Cliente> retornaList() {
		return clienteDAO.retornaLista();
	}

	public Cliente retornaCliente(Integer numConta) {
		return clienteDAO.retornaCliente(numConta);
	}

	public void depositar(Double valor, Integer numConta) {
		clienteDAO.depositar(valor, numConta);
	}

	public String sacar(Double valor, Integer numConta) {
		Cliente cliente = clienteDAO.retornaCliente(numConta);
		if (cliente.getSaldo() > valor) {
			clienteDAO.sacar(valor, numConta);
			return null;
		} else {
			return "Saldo insuficiente";
		}
	}

	public String bloquear(Integer numConta) {
		Cliente cliente = clienteDAO.retornaCliente(numConta);
		EstadoConta estado = EstadoConta.valueOf("BLOQUEADA");
		cliente.setEstado(estado);
		return "CONTA BLOQUEADA";
	}
	public String desbloquear(Integer numConta) {
		Cliente cliente = clienteDAO.retornaCliente(numConta);
		EstadoConta estado = EstadoConta.valueOf("ATIVO");
		cliente.setEstado(estado);
		return "CONTA DESBLOQUEADA";
	}
	public String encerrar(Integer numConta) {
		Cliente cliente = clienteDAO.retornaCliente(numConta);
		clienteDAO.sacar(cliente.getSaldo(), numConta);
		EstadoConta estado = EstadoConta.valueOf("ENCERRADA");
		cliente.setEstado(estado);
		return "CONTA ENCERRADA";
	}
}
