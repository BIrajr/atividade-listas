package br.ucsal.bes20182.poo.banco.estado;

public enum EstadoConta {
	ATIVO, BLOQUEADA, ENCERRADA;
}
