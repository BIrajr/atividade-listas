package br.ucsal.bes20182.poo.banco.domain;

import br.ucsal.bes20182.poo.banco.estado.EstadoConta;

public class Cliente {
	private String nome;
	private String cpf;
	private String telefone;
	private String endereco;
	private Double rendaMensal;
	private Integer numeroConta;
	private Double saldo = 0D;
	private EstadoConta estado = EstadoConta.ATIVO;
	private Integer limite;

	public Integer getLimite() {
		return limite;
	}

	public void setLimite(Integer limite) {
		this.limite = limite;
	}

	public EstadoConta getEstado() {
		return estado;
	}

	public void setEstado(EstadoConta estado) {
		this.estado = estado;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public Cliente(String nome, String cpf, String telefone, String endereco, Double rendaMensal, Integer numeroConta) {
		this.nome = nome;
		this.cpf = cpf;
		this.telefone = telefone;
		this.endereco = endereco;
		this.rendaMensal = rendaMensal;
		this.numeroConta = numeroConta;
	}

	public Integer getNumeroConta() {
		return numeroConta;
	}

	public void setNumeroConta(Integer numeroConta) {
		this.numeroConta = numeroConta;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public Double getRendaMensal() {
		return rendaMensal;
	}

	public void setRendaMensal(Double rendaMensal) {
		this.rendaMensal = rendaMensal;
	}
}
