package br.ucsal.bes20182.poo.banco.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20182.poo.banco.domain.Cliente;

public class ClienteDAO {
	public List<Cliente> clientes = new ArrayList<>();

	public void registrar(Cliente cliente) {
		clientes.add(cliente);
	}

	public List<Cliente> retornaLista() {
		return clientes;
	}

	public Cliente retornaCliente(Integer numConta) {
		return clientes.get(numConta - 1);
	}

	public void depositar(Double valor, Integer numConta) {
		clientes.get(numConta - 1).setSaldo(valor + clientes.get(numConta - 1).getSaldo());
	}

	public void sacar(Double valor, Integer numConta) {
		clientes.get(numConta - 1).setSaldo(clientes.get(numConta - 1).getSaldo() - valor);
	}

}
