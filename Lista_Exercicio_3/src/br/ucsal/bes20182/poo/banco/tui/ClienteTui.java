package br.ucsal.bes20182.poo.banco.tui;

import java.util.List;
import java.util.Scanner;

import br.ucsal.bes20182.poo.banco.business.ClienteBO;
import br.ucsal.bes20182.poo.banco.domain.Cliente;

public class ClienteTui {
	public Scanner sc = new Scanner(System.in);
	public ClienteBO clienteBO = new ClienteBO();
	public Integer num = 1;

	public void adicionar() {
		System.out.println("######### CADASTRO NO BANCO DO POVAO #########");
		String nome = obterString("Informe o nome: ");
		String cpf = obterString("Informe o cpf: ");
		String telefone = obterString("Informe o telefone: ");
		String endereco = obterString("Informe o endereco: ");
		Double rendaMensal = obterDouble("Informe a renda mensal");
		sc.nextLine();
		Integer numeroConta = null;
		Cliente cliente = new Cliente(nome, cpf, telefone, endereco, rendaMensal, numeroConta);
		validarConta("Entregou todos os documentos necessarios? (RG, Comprovante de Residencia e comprovante de renda): ",cliente);
		Integer limite  = limiteCredito("Deseja limite de credito ? ");
		cliente.setLimite(limite);
	}

	private Integer limiteCredito(String mensagem) {
		System.out.println(mensagem);
		String resp = sc.nextLine();
		if(resp.equals("S") || resp.equals("Sim") || resp.equals("sim") || resp.equals("SIM")) {
			System.out.println("Qual o limite de credito ? ");
			return sc.nextInt();
		}
		else {
			return null;
		}
	}

	public void validarConta(String mensagem, Cliente cliente) {
		System.out.println(mensagem);
		String resp = sc.nextLine();
		String val;
		if (resp.equals("S") || resp.equals("Sim") || resp.equals("sim") || resp.equals("SIM")) {
			cliente.setNumeroConta(num);
			num++;
			val = clienteBO.registrar(cliente, true);
			System.out.println(val + " Numero da Conta: " + cliente.getNumeroConta());

		} else {
			val = clienteBO.registrar(cliente, false);
			System.out.println(val);
			validarConta("Entregou todos os documentos necessarios? (RG, Comprovante de Residencia e comprovante de renda): ",cliente);
		}

	}

	private Double obterDouble(String mensagem) {
		System.out.println(mensagem);
		return sc.nextDouble();
	}

	private String obterString(String mensagem) {
		System.out.println(mensagem);
		return sc.nextLine();
	}
	public void listar() {

		List<Cliente> clientes = clienteBO.retornaList();
		for(Cliente cliente: clientes) {
			System.out.println("Saldo do SR." + cliente.getNome() + " + cliente.getSaldo()");
		}
	}
	
	public void checarSaldo(Integer numConta) {
		System.out.println("######### SALDO #########");
		Cliente cliente = clienteBO.retornaCliente(numConta);
		System.out.println("Saldo da conta do SR. " + cliente.getNome()+ " : R$" + cliente.getSaldo());
	}
	public void depositar(Integer numConta) {
		System.out.println("######### DEPOSITO #########");
		System.out.println("Valor a depositar: ");
		Double valor = sc.nextDouble();
		sc.nextLine();
		clienteBO.depositar(valor, numConta);
		System.out.println("Deposito feito com sucesso!");
	}
	public void saque(Integer numConta) {
		System.out.println("######### SAQUE ##########");
		System.out.println("Valor a sacar: ");
		Double valor = sc.nextDouble();
		sc.nextLine();
		String erro =  clienteBO.sacar(valor, numConta);
		if(erro == null) {
		System.out.println("Saque feito com sucesso!");
		}
		else {
			System.out.println(erro);
		}
	}
	public void bloquear (Integer numConta) {
		String bloq = clienteBO.bloquear(numConta);
		System.out.println(bloq);
	}
	public void desbloquear (Integer numConta) {
		String desbloq = clienteBO.desbloquear(numConta);
		System.out.println(desbloq);
	}
	public void encerrar(Integer numConta) {
		String enc = clienteBO.encerrar(numConta);
		System.out.println(enc);
	}
}
