package br.ucsal.bes20182.poo.banco.tui;

public class BancoTui {
	public static void main(String[] args) {
		ClienteTui clienteTui = new ClienteTui();
		clienteTui.adicionar();
		clienteTui.depositar(1);
		clienteTui.saque(1);
		clienteTui.checarSaldo(1);
		clienteTui.bloquear(1);
		clienteTui.desbloquear(1);
		clienteTui.encerrar(1);
		clienteTui.checarSaldo(1);
	}
}
